const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const order = new Schema({
    _id: mongoose.Types.ObjectId,
    orderCode: {
        type: String,
        unique: true
    },
    pizzaSize: {
        type: String,
        required: true
    },
    pizzaType: {
        type: String,
        required: true
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: "vouchers"
    },
    drink: {
        type: mongoose.Types.ObjectId,
        ref: "drink"
    },
    status: {
        type: String,
        required: true
    },
}, {
    timestamps: {
        createdAt: "ngayTao",
        updatedAt: "ngayCapNhat"
    }
});

module.exports = mongoose.model("orders", order);