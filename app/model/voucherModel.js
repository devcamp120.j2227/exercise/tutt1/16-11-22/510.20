const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const voucher = new Schema({
    _id: mongoose.Types.ObjectId,
    maVoucher: {
        type: String,
        required: true,
        unique: true
    },
    phanTramGiamGia: {
        type: Number,
        required: true
    },
    ghiChu: {
        type: String,
        required: false
    },
}, {
    timestamps: true
});

module.exports = mongoose.model("vouchers", voucher);