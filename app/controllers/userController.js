const mongoose = require("mongoose");
const UserModel = require("../model/userModel");

const createUser = (req, res) => {
    // B1: Thu thập dữ liệu
    let body = req.body;
    // B2:Kiểm tra dữ liệu
    if(!body.fullName) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "fullName is required"
        });
    }
    if(!body.email) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "email is required"
        });
    }
    if(!body.address) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "address is required"
        });
    }
    if(!body.phone) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "phone is required"
        });
    }
    // B3: Xử lý và hiển thị kết quả
    let newUser = {
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        email: body.email,
        address: body.address,
        phone: body.phone,
        orders: body.orders
    }
    UserModel.create(newUser, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            });
        } else {
            return res.status(201).json({
                status: "Successfully create user",
                users: data
            });
        }
    });
}

const getAllUser = (req, res) => {
    // Thu thập dữ liệu

    // Kiểm tra dữ liệu

    // Xử lý và hiển thị kết quả
    UserModel.find((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            });
        } else {
            return res.status(200).json({
                status: "Successfully get all user",
                users: data
            });
        }
    });
}

const getUserById = (req, res) => {
    // Thu thập dữ liệu
    let userId = req.params.userId;
    // Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is required"
        });
    }
    // Xử lý và hiển thị kết quả
    UserModel.findById(userId, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            });
        } else {
            return res.status(200).json({
                status: "Successfully get user",
                users: data
            });
        }
    });
}

const updateUserById = (req, res) => {
    // B1: Thu thập dữ liệu
    let userId = req.params.userId;
    let body = req.body;
    // B2:Kiểm tra dữ liệu
    if(!body.fullName) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "fullName is required"
        });
    }
    if(!body.email) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "email is required"
        });
    }
    if(!body.address) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "address is required"
        });
    }
    if(!body.phone) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "phone is required"
        });
    }
    // B3: Xử lý và hiển thị kết quả
    let newUser = {
        fullName: body.fullName,
        email: body.email,
        address: body.address,
        phone: body.phone,
        orders: body.orders
    }
    UserModel.findByIdAndUpdate(userId, newUser, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            });
        } else {
            return res.status(201).json({
                status: "Successfully update user",
                users: data
            });
        }
    });
}

const deleteUserById = (req, res) => {
    // Thu thập dữ liệu
    let userId = req.params.userId;
    // Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is required"
        });
    }
    // Xử lý và hiển thị kết quả
    UserModel.findByIdAndDelete(userId, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            });
        } else {
            return res.status(204).json({
                status: "Successfully delete user",
                users: data
            });
        }
    });
}

const getAllUserLimit = (req, res) => {
    // Thu thập dữ liệu
    let limit = req.query.limit;
    // Kiểm tra dữ liệu

    // Xử lý và hiển thị kết quả
    UserModel.find().limit(limit).exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            });
        } else {
            return res.status(200).json({
                status: "Successfully get user",
                users: data
            });
        }
    })
}

const getAllUserSkip = (req, res) => {
    // Thu thập dữ liệu
    let skip = req.query.skip;
    // Kiểm tra dữ liệu

    // Xử lý và hiển thị kết quả
    UserModel.find().skip(skip).exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            });
        } else {
            return res.status(200).json({
                status: "Successfully get user",
                users: data
            });
        }
    })
}

const getAllUserSorted = (req, res) => {
    // Thu thập dữ liệu
    let fullName = req.query.fullName;
    let condition = {};
    // Kiểm tra dữ liệu
    if(fullName) {
        condition.fullName = fullName
    }
    // Xử lý và hiển thị kết quả
    UserModel.find(condition).sort({"fullName": "asc"}).exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            });
        } else {
            return res.status(200).json({
                status: "Successfully get user",
                users: data
            });
        }
    })
}

const getAllUserSkipLimit = (req, res) => {
    // Thu thập dữ liệu
    let limit = req.query.limit;
    let skip = req.query.skip;
    // Kiểm tra dữ liệu

    // Xử lý và hiển thị kết quả
    UserModel.find().limit(limit).skip(skip).exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            });
        } else {
            return res.status(200).json({
                status: "Successfully get user",
                users: data
            });
        }
    })
}

const getAllUserSortSkipLimit = (req, res) => {
    // Thu thập dữ liệu
    let limit = req.query.limit;
    let skip = req.query.skip;
    let fullName = req.query.fullName;
    let condition = {};
    // Kiểm tra dữ liệu
    if(fullName) {
        condition.fullName = fullName
    }
    // Xử lý và hiển thị kết quả
    UserModel.find(condition).sort({"fullName": "asc"}).limit(limit).skip(skip).exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            });
        } else {
            return res.status(200).json({
                status: "Successfully get user",
                users: data
            });
        }
    })
}

module.exports = { createUser, getAllUser, getUserById, updateUserById, deleteUserById, getAllUserLimit, getAllUserSkip, getAllUserSorted, getAllUserSkipLimit, getAllUserSortSkipLimit }