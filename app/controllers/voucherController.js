const mongoose = require("mongoose");
const voucherModel = require("../model/voucherModel");

const getAllVouchers = (req, res) => {
    // B1: Thu thập dữ liệu - bỏ qua

    // B2: Kiểm tra dữ liệu - bỏ qua

    // B3: Xử lý và hiển thị kết quả
    voucherModel.find((error, data) => {
        if(error) {
            return res.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return res.status(200).json({
                message: `Successfully get all voucher`,
                voucher: data
            });
        }
    });
}

const createVoucher = (req, res) => {
    // B1: Thu thập dữ liệu
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if(!body.maVoucher) {
        return res.status(400).json({
            message: `maVoucher is required`
        });
    }
    if(!body.phanTramGiamGia) {
        return res.status(400).json({
            message: `phanTramGiamGia is required`
        })
    }
    if(!Number.isInteger(body.phanTramGiamGia)) {
        return res.status(400).json({
            message: `phanTramGiamGia is invalid`
        });
    }
    // B3: Xử lý và hiển thị kết quả
    let newVoucher = new voucherModel({
        _id: mongoose.Types.ObjectId(),
        maVoucher: body.maVoucher,
        phanTramGiamGia: body.phanTramGiamGia,
        ghiChu: body.ghiChu
    });
    voucherModel.create(newVoucher, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return res.status(201).json({
                message: `Successfully create voucher`,
                voucher: data
            });
        }
    });
}

const getVoucherById = (req, res) => {
    // B1: Thu thập dữ liệu
    let voucherId = req.params.voucherId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: `voucherId is invalid`
        });
    }
    // B3: Xử lý và hiển thị kết quả
    voucherModel.findById(voucherId, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return res.status(200).json({
                message: `Successfully get voucher`,
                voucher: data
            });
        }
    });
}

const updateVoucherById = (req, res) => {
    // B1: Thu thập dữ liệu
    let voucherId = req.params.voucherId;
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: `voucherId is invalid`
        });
    }
    if(!body.maVoucher) {
        return res.status(400).json({
            message: `maVoucher is required`
        });
    }
    if(!body.phanTramGiamGia) {
        return res.status(400).json({
            message: `phanTramGiamGia is required`
        });
    }
    if(!Number.isInteger(body.phanTramGiamGia)) {
        return res.status(400).json({
            message: `phanTramGiamGia is invalid`
        });
    }
    // B3: Xử lý và hiển thị kết quả
    voucherModel.findByIdAndUpdate(voucherId, body, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return res.status(200).json({
                message: `Successfully update voucher`,
                voucher: data
            });
        }
    });
}

const deleteVoucherById = (req, res) => {
    // B1: Thu thập dữ liệu
    let voucherId = req.params.voucherId;
    // B2: Kiểm tra dữ liêu
    if(!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: `voucherId is invalid`
        });
    }
    // B3: Xử lý và hiển thị kết quả
    voucherModel.findByIdAndDelete(voucherId, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return res.status(200).json({
                message: `Successfully delete voucher`,
                voucher: data
            });
        }
    });
}

module.exports = { createVoucher, getAllVouchers, getVoucherById, updateVoucherById, deleteVoucherById }