const mongoose = require("mongoose");
const OrderModel = require("../model/orderModel");
const userModel = require("../model/userModel");
const { updateUserById } = require("../controllers/userController")

const createOrder = (req, res) => {
    // Thu thập dữ liệu
    let userId = req.params.userId;
    let body = req.body;
    // Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is required"
        });
    }
    if(!body.pizzaSize) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "pizzaSize is required"
        });
    }
    if(!body.pizzaType) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "pizzaType is required"
        });
    }
    if(!body.status) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "status is required"
        });
    }
    // Xử lý và hiển thị kết quả
    let newOrder = {
        _id: mongoose.Types.ObjectId(),
        orderCode: body.orderCode,
        pizzaSize: body.pizzaSize,
        pizzaType: body.pizzaType,
        voucher: body.voucher,
        drink: body.drink,
        status: body.status
    }
    OrderModel.create(newOrder, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            });
        } else {
            userModel.findByIdAndUpdate(userId, 
                {
                    $push: { orders: data._id }
                },
                (error, updateUserById) => {
                    if(error) {
                        return res.status(500).json({
                            status: "Error 500: Internal server error",
                            message: error.message
                        });
                    } else {
                        return res.status(201).json({
                            status: "Successfully create order",
                            orders: data
                        });
                    }
                }
            )
        }
    })
}

const getAllOrder = (req, res) => {
    // Thu thập dữ liệu
    // Kiểm tra dữ liệu
    // Xử lý và hiển thị kết quả
    OrderModel.find((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            });
        } else {
            return res.status(200).json({
                status: "Successfully get all order",
                orders: data
            });
        }
    });
}

const getOrderById = (req, res) => {
    // Thu thập dữ liệu
    let orderId = req.params.orderId;
    // Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "orderId is required"
        });
    }
    // Xử lý và hiển thị kết quả
    OrderModel.findById(orderId, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            });
        } else {
            return res.status(200).json({
                status: "Successfully get order",
                users: data
            });
        }
    });
}

const updateOrderById = (req, res) => {
    // B1: Thu thập dữ liệu
    let orderId = req.params.orderId;
    let body = req.body;
    // B2:Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "orderId is required"
        });
    }
    if(!body.pizzaSize) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "pizzaSize is required"
        });
    }
    if(!body.pizzaType) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "pizzaType is required"
        });
    }
    if(!body.status) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "status is required"
        });
    }
    // B3: Xử lý và hiển thị kết quả
    let newOrder = {
        orderCode: body.orderCode,
        pizzaSize: body.pizzaSize,
        pizzaType: body.pizzaType,
        voucher: body.voucher,
        drink: body.drink,
        status: body.status
    }
    OrderModel.findByIdAndUpdate(orderId, newOrder, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            });
        } else {
            return res.status(201).json({
                status: "Successfully update order",
                orders: data
            });
        }
    });
}

const deleteOrderById = (req, res) => {
    // Thu thập dữ liệu
    let orderId = req.params.orderId;
    // Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "orderId is required"
        });
    }
    // Xử lý và hiển thị kết quả
    OrderModel.findByIdAndDelete(orderId, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            });
        } else {
            return res.status(204).json({
                status: "Successfully delete order",
                orders: data
            });
        }
    });
}

module.exports = { createOrder, getAllOrder, getOrderById, updateOrderById, deleteOrderById }