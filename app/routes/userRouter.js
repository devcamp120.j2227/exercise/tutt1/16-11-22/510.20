const express = require("express");
const router = express.Router();
const { createUser, getAllUser, getUserById, updateUserById, deleteUserById, getAllUserLimit, getAllUserSkip, getAllUserSorted, getAllUserSkipLimit, getAllUserSortSkipLimit } = require("../controllers/userController");

router.get("/users", getAllUser);
router.get("/users/:userId", getUserById);
router.post("/users", createUser);
router.put("/users/:userId", updateUserById);
router.delete("/users/:userId", deleteUserById);

router.get("/limit-users", getAllUserLimit);
router.get("/skip-users", getAllUserSkip);
router.get("/sort-users", getAllUserSorted);
router.get("/skip-limit-users", getAllUserSkipLimit);
router.get("/sort-skip-limit-users", getAllUserSortSkipLimit);

module.exports = router;