const express = require("express");
const { createVoucher, getAllVouchers, getVoucherById, updateVoucherById, deleteVoucherById } = require("../controllers/voucherController");
const router = express.Router();

router.get("/voucher", getAllVouchers);
router.get("/voucher/:voucherId", getVoucherById);
router.post("/voucher", createVoucher);
router.put("/voucher/:voucherId", updateVoucherById)
router.delete("/voucher/:voucherId", deleteVoucherById);

module.exports = router;