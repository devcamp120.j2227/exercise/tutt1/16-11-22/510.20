const express = require("express");
const { createOrder, getAllOrder, getOrderById, updateOrderById, deleteOrderById } = require("../controllers/orderController");
const router = express.Router();

router.get("/orders", getAllOrder);
router.get("/orders/:orderId", getOrderById);
router.post("/users/:userId/orders", createOrder);
router.put("/orders/:orderId", updateOrderById);
router.delete("/orders/:orderId", deleteOrderById);

module.exports = router;